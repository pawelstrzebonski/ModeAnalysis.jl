@testset "rms_optim.jl" begin
    modes = reduce(hcat, [sin.((0:0.1:1) .* i .* pi) .^ 2 for i = 1:3])
    modalpowers = [1, 2, 3]
    nearfields = ModeAnalysis.msum(modes, modalpowers)
    modalpowers2 = ModeAnalysis.modal_decomposition_covariance(modes, nearfields)
    @test isapprox(
        modalpowers2 ./ sum(modalpowers2),
        modalpowers ./ sum(modalpowers),
        rtol = 0.1,
    )

    modes = cat(
        [
            (sin.((0:0.1:1) .* i .* pi) .^ 2) * (sin.((0:0.1:1) .* i .* pi) .^ 2)' for
            i = 1:3
        ]...,
        dims = 3,
    )
    modalpowers = [1, 2, 3]
    nearfields = ModeAnalysis.msum(modes, modalpowers)
    modalpowers2 = ModeAnalysis.modal_decomposition_covariance(modes, nearfields)
    @test isapprox(
        modalpowers2 ./ sum(modalpowers2),
        modalpowers ./ sum(modalpowers),
        rtol = 0.1,
    )

    modes = reduce(hcat, [sin.((0:0.1:1) .* i .* pi) .^ 2 for i = 1:3])
    modesff = reduce(hcat, [cos.((0:0.1:1) .* i .* pi) .^ 2 for i = 1:3])
    modalpowers = [1, 2, 3]
    nearfields = ModeAnalysis.msum(modes, modalpowers)
    farfields = ModeAnalysis.msum(modesff, modalpowers)
    modalpowers2 =
        ModeAnalysis.modal_decomposition_covariance(modes, nearfields, modesff, farfields)
    @test isapprox(
        modalpowers2 ./ sum(modalpowers2),
        modalpowers ./ sum(modalpowers),
        rtol = 0.1,
    )

    modes = cat(
        [
            (sin.((0:0.1:1) .* i .* pi) .^ 2) * (sin.((0:0.1:1) .* i .* pi) .^ 2)' for
            i = 1:3
        ]...,
        dims = 3,
    )
    modesff = cat(
        [
            (cos.((0:0.1:1) .* i .* pi) .^ 2) * (cos.((0:0.1:1) .* i .* pi) .^ 2)' for
            i = 1:3
        ]...,
        dims = 3,
    )
    modalpowers = [1, 2, 3]
    nearfields = ModeAnalysis.msum(modes, modalpowers)
    farfields = ModeAnalysis.msum(modesff, modalpowers)
    modalpowers2 =
        ModeAnalysis.modal_decomposition_covariance(modes, nearfields, modesff, farfields)
    @test isapprox(
        modalpowers2 ./ sum(modalpowers2),
        modalpowers ./ sum(modalpowers),
        rtol = 0.1,
    )
end
