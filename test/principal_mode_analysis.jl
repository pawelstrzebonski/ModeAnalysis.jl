@testset "principal_mode_analysis.jl" begin
    modes = reduce(hcat, [sin.((0:0.1:1) .* i .* pi) .^ 2 for i = 1:3])
    modalpowers = rand(3, 30)
    nearfields = reduce(
        hcat,
        [ModeAnalysis.msum(modes, m) for m in eachslice(modalpowers, dims = 2)],
    )
    @test isapprox(ModeAnalysis.mode_recovery(nearfields, modalpowers), modes)

    modes = cat(
        [
            (sin.((0:0.1:1) .* i .* pi) .^ 2) * (sin.((0:0.1:1) .* i .* pi) .^ 2)' for
            i = 1:3
        ]...,
        dims = 3,
    )
    modalpowers = rand(3, 30)
    nearfields = cat(
        [ModeAnalysis.msum(modes, m) for m in eachslice(modalpowers, dims = 2)]...,
        dims = 3,
    )
    @test isapprox(ModeAnalysis.mode_recovery(nearfields, modalpowers), modes)
end
