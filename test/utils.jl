@testset "utils.jl" begin
    @test ModeAnalysis.mean([1, 2, 3]) == 2

    @test ModeAnalysis.rms([1, 2, 3], [1, 2, 3]) == 0
    @test ModeAnalysis.rms([1, 2, 3], [2, 1, 4]) == 1

    modes = [1 0 0 0; 0 1 0 0; 0 0 1 0]'
    pows = [1, 2, 3]
    @test ModeAnalysis.msum(modes, pows) == [1, 2, 3, 0]
    pows = [3, 0, 3]
    @test ModeAnalysis.msum(modes, pows) == [3, 0, 3, 0]

    modes = zeros(2, 2, 3)
    modes[1, 1, 1] = modes[2, 2, 2] = modes[1, 2, 3] = modes[2, 1, 3] = 1
    pows = [1, 2, 3]
    @test ModeAnalysis.msum(modes, pows) == [1 3; 3 2]
    pows = [3, 0, 3]
    @test ModeAnalysis.msum(modes, pows) == [3 3; 3 0]
end
