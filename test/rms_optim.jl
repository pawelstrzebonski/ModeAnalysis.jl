@testset "rms_optim.jl" begin
    modes = reduce(hcat, [sin.((0:0.1:1) .* i .* pi) .^ 2 for i = 1:3])
    modalpowers = [1, 2, 3]
    nearfields = ModeAnalysis.msum(modes, modalpowers)
    @test isapprox(
        ModeAnalysis.modal_decomposition_rms(modes, nearfields),
        modalpowers,
        rtol = 0.01,
    )

    modes = cat(
        [
            (sin.((0:0.1:1) .* i .* pi) .^ 2) * (sin.((0:0.1:1) .* i .* pi) .^ 2)' for
            i = 1:3
        ]...,
        dims = 3,
    )
    modalpowers = [1, 2, 3]
    nearfields = ModeAnalysis.msum(modes, modalpowers)
    @test isapprox(ModeAnalysis.modal_decomposition_rms(modes, nearfields), modalpowers)


    modes = reduce(hcat, [sin.((0:0.1:1) .* i .* pi) .^ 2 for i = 1:3])
    modesff = reduce(hcat, [cos.((0:0.1:1) .* i .* pi) .^ 2 for i = 1:3])
    modalpowers = [1, 2, 3]
    nearfields = ModeAnalysis.msum(modes, modalpowers)
    farfields = ModeAnalysis.msum(modesff, modalpowers)
    @test isapprox(
        ModeAnalysis.modal_decomposition_rms(modes, nearfields, modesff, farfields),
        modalpowers,
        rtol = 0.01,
    )

    modes = cat(
        [
            (sin.((0:0.1:1) .* i .* pi) .^ 2) * (sin.((0:0.1:1) .* i .* pi) .^ 2)' for
            i = 1:3
        ]...,
        dims = 3,
    )
    modesff = cat(
        [
            (cos.((0:0.1:1) .* i .* pi) .^ 2) * (cos.((0:0.1:1) .* i .* pi) .^ 2)' for
            i = 1:3
        ]...,
        dims = 3,
    )
    modalpowers = [1, 2, 3]
    nearfields = ModeAnalysis.msum(modes, modalpowers)
    farfields = ModeAnalysis.msum(modesff, modalpowers)
    @test isapprox(
        ModeAnalysis.modal_decomposition_rms(modes, nearfields, modesff, farfields),
        modalpowers,
        rtol = 0.01,
    )
end
