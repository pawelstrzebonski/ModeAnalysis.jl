# ModeAnalysis

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://pawelstrzebonski.gitlab.io/ModeAnalysis.jl)
[![Build Status](https://gitlab.com/pawelstrzebonski/ModeAnalysis.jl/badges/master/pipeline.svg)](https://gitlab.com/pawelstrzebonski/ModeAnalysis.jl/pipelines)
[![Coverage](https://gitlab.com/pawelstrzebonski/ModeAnalysis.jl/badges/master/coverage.svg)](https://gitlab.com/pawelstrzebonski/ModeAnalysis.jl/commits/master)

## About

`ModeAnalysis.jl` is a package for analyzing (mainly laser) modes.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/ModeAnalysis.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for ModeAnalysis.jl](https://pawelstrzebonski.gitlab.io/ModeAnalysis.jl/).
