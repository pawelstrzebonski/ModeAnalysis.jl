# Example Usage

## Modal Profile Recovery

If we are able to measure a set of multi-moded images as well as the
corresponding modal power coefficients (that is to say, we know the
amount of power in each of the transverse modes in any of the given
images), then we can use least-squares to estimate the profiles of the
individual modes:

```julia
import ModeAnalysis

# Load images as an N-D array `nearfields`
# Load modal power coefficients as a 2D array `modalpowers`

# We obtain the modal profiles as an N-D array
modeprofiles=ModeAnalysis.mode_recovery(nearfields, modalpowers)
```

## Modal Decomposition

Now, if we know the modal profiles of the individual modes (perhaps from
prior direct measurement, modal recovery, or theory/simulation) then
we can use modal decomposition to estimate what modes are present in
a multi-moded image and in what proportion of power. These are obtained
using an optimization routine. This package (as of writing) provides
two routines for modal decomposition that differ in the method of
calculating the optimization error function:

```julia
import ModeAnalysis

# Load individual mode profiles as an N-D array `modes_nf`
# Load multi-moded images as an N-D array `nf`

# We obtain the modal power coefficients as an array using the covariance method
modalpowers=ModeAnalysis.modal_decomposition_covariance(modes_nf, nf)

# We obtain the modal power coefficients as an array using the RMS method
modalpowers=ModeAnalysis.modal_decomposition_rms(modes_nf, nf)
```

The covariance method will tend to be faster than the RMS method as it
only optimizes for the correct relative modal powers, not the absolute
modal power coefficients needed to reconstruct the multi-moded
image from the provided individual mode profiles.

Now, in some cases the modal decomposition problem is prone to error
(in particular when the modal intensity profiles of modes are rather
similar). In these cases the decomposition can be improved by providing
far-field images and modal profiles in addition to the near-field images
and profiles:

```julia
import ModeAnalysis

# Load individual mode profiles as an N-D array `modes_nf`
# Load multi-moded images as an N-D array `nf`
# Load individual mode far-field profiles as an N-D array `modes_ff`
# Load multi-moded far-field images as an N-D array `ff`

# We obtain the modal power coefficients as an array using the covariance method
modalpowers=ModeAnalysis.modal_decomposition_covariance(modes_nf, nf, modes_ff, ff)

# We obtain the modal power coefficients as an array using the RMS method
modalpowers=ModeAnalysis.modal_decomposition_rms(modes_nf, nf, modes_ff, ff)
```
