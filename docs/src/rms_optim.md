# rms_optim.jl

## Description

Implements modal decomposition by optimizing with a RMS error function.

## Functions

```@autodocs
Modules = [ModeAnalysis]
Pages   = ["rms_optim.jl"]
```
