# principal\_mode\_analysis.jl

## Description

Implements least-squares based modal profile recovery.

## Functions

```@autodocs
Modules = [ModeAnalysis]
Pages   = ["principal_mode_analysis.jl"]
```
