# covariance_optim.jl

## Description

Implements modal decomposition by optimizing with a covariance error function.

## Functions

```@autodocs
Modules = [ModeAnalysis]
Pages   = ["covariance_optim.jl"]
```
