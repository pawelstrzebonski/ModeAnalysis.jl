# ModeAnalysis.jl Documentation

`ModeAnalysis.jl` is a package for analyzing (mainly laser) modes.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/ModeAnalysis.jl
```

## Status

* Basic modal decomposition implemented (RMS and covariance optimization methods)
* Modal decomposition using both near-field and near-field+far-field measurements
* Least-squares fit based principal mode analysis aka modal profile recovery (given near-fields and corresponding modal power coefficients)
* Works on 1D and 2D images


## Related Packages

* [ModeAnalysisML.jl](https://gitlab.com/pawelstrzebonski/ModeAnalysisML.jl) for similar modal analysis using machine learning methods
* [LASERAnalysis.jl](https://gitlab.com/pawelstrzebonski/LASERAnalysis.jl) for basic laser analysis, including identifying and clustering spectral modes for series of optical spectra
