# utils.jl

## Description

Basic functions used throughout this package.

## Functions

```@autodocs
Modules = [ModeAnalysis]
Pages   = ["utils.jl"]
```
