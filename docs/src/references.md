# References

Some of the basic modal decomposition algorithms implemented in this
package are based on their descriptions in
["Comparative analysis of numerical methods for the mode analysis of laser beams"](https://doi.org/10.1364/AO.52.007769).
