using Documenter
import ModeAnalysis

makedocs(
    sitename = "ModeAnalysis.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "ModeAnalysis.jl"),
    pages = [
        "Home" => "index.md",
        "Example Usage" => "example.md",
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "covariance_optim.jl" => "covariance_optim.md",
            "principal\\_mode\\_analysis.jl" => "principal_mode_analysis.md",
            "rms_optim.jl" => "rms_optim.md",
            "utils.jl" => "utils.md",
        ],
        "test/" => [
            "covariance_optim.jl" => "covariance_optim_test.md",
            "principal\\_mode\\_analysis.jl" => "principal_mode_analysis_test.md",
            "rms_optim.jl" => "rms_optim_test.md",
            "utils.jl" => "utils_test.md",
        ],
    ],
)
