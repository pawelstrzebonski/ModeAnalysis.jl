import Optim

"""
    modal_decomposition_rms(modes_nf, nf[, modes_ff, ff])

Given a set of modal near-fields `modes_nf` and near-field image `nf`,
determine the modal power composition by minimizing RMS error. Adding
modal far-fields and far-field image improves accuracy and can avoid
spurious results for systems with many modes.

# Arguments
- `modes_nf::AbstractArray`: modal intensity profiles of the individual modes.
- `nf::AbstractArray`: multi-moded near-field intensity profiles.
- `modes_ff::AbstractArray`: modal far-field intensity profiles of the individual modes.
- `ff::AbstractArray`: multi-moded far-field intensity profiles.
"""
function modal_decomposition_rms(modes_nf::AbstractArray, nf::AbstractArray)
    @assert ndims(modes_nf) == ndims(nf) + 1
    @assert size(modes_nf)[1:end-1] == size(nf)
    N = size(modes_nf)[end]
    errfun(x) = rms(msum(modes_nf, x), nf)
    results = Optim.optimize(errfun, zeros(N))
    results.minimizer
end
function modal_decomposition_rms(
    modes_nf::AbstractArray,
    nf::AbstractArray,
    modes_ff::AbstractArray,
    ff::AbstractArray,
)
    @assert ndims(modes_nf) == ndims(nf) + 1
    @assert ndims(modes_ff) == ndims(ff) + 1
    @assert size(modes_nf)[1:end-1] == size(nf)
    @assert size(modes_ff)[1:end-1] == size(ff)
    N = size(modes_nf)[end]
    errfun(x) = rms(msum(modes_nf, x), nf) + rms(msum(modes_ff, x), ff)
    results = Optim.optimize(errfun, zeros(N))
    results.minimizer
end
