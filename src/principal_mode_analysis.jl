"""
    mode_recovery(nearfields, modalpowers)

Given a set of near-field images `nearfields` and corresponding modal
power measurements `modalpowers` for each image, use least-square fit
to recover the modal near-fields.
"""
function mode_recovery(nearfields::AbstractArray, modalpowers::AbstractMatrix)
    @assert size(nearfields)[end] == size(modalpowers)[end]
    N_modes = size(modalpowers)[1]
    N_images = size(nearfields)[end]
    # Calculate the LSQ fitting matrix
    lsfMat = modalpowers' * inv(modalpowers * modalpowers')
    # Apply LSQ to generate the modal near-fields
    modal_nearfields = reshape(nearfields, :, N_images) * lsfMat
    # Reshape to the original image shapes
    modal_nearfields = reshape(modal_nearfields, size(nearfields)[1:end-1]..., N_modes)
    modal_nearfields
end
