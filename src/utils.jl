"""
    msum(modes, vec)

Find them modal field given principal `modes` and the coefficient of
each mode `vec`.
"""
function msum(modes::AbstractArray{F,3}, vec::AbstractVector) where {F<:Number}
    @assert size(modes)[end] == length(vec) "`vec` must be length of the number of modes"
    A = zeros(size(modes)[1:end-1])
    for i = 1:size(modes)[end]
        A .+= vec[i] .* modes[:, :, i]
    end
    A
end
function msum(modes::AbstractMatrix, vec::AbstractVector)
    @assert size(modes)[end] == length(vec) "`vec` must be length of the number of modes"
    A = zeros(size(modes)[1:end-1])
    for i = 1:size(modes)[end]
        A .+= vec[i] .* modes[:, i]
    end
    A
end

"""
    rms(A, B)

Root-Mean-Square error between arrays `A` and `B`.
"""
function rms(A::AbstractArray, B::AbstractArray)
    @assert size(A) == size(B) "Arrays must be of equal size"
    sqrt(sum(abs2.(A .- B)) / length(A[:]))
end

"""
    mean(x)

Arithmetic average.
"""
mean(x::AbstractArray) = sum(x) / length(x[:])
