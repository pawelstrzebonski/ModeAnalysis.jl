import Optim

#TODO: precision seems worse than for RMS optimization (convergence condition?)

"""
    modal_decomposition_covariance(modes_nf, nf[, modes_ff, ff])

Given a set of modal near-fields `modes_nf` and near-field image `nf`,
determine the modal power composition by maximizing covariance. Adding
modal far-fields and far-field image improves accuracy and can avoid
spurious results for systems with many modes.

# Arguments
- `modes_nf::AbstractArray`: modal intensity profiles of the individual modes.
- `nf::AbstractArray`: multi-moded near-field intensity profiles.
- `modes_ff::AbstractArray`: modal far-field intensity profiles of the individual modes.
- `ff::AbstractArray`: multi-moded far-field intensity profiles.
"""
function modal_decomposition_covariance(modes_nf::AbstractArray, nf::AbstractArray)
    @assert length(size(modes_nf)) == length(size(nf)) + 1
    @assert size(modes_nf)[1:end-1] == size(nf)
    N = size(modes_nf)[end]
    function errfun(x)
        nf_rec = msum(abs2.(modes_nf), x)
        Delta_nf_m = nf .- mean(nf)
        Delta_nf_r = nf_rec .- mean(nf_rec)
        y =
            1 - abs(
                sum(Delta_nf_m .* Delta_nf_r) /
                sqrt(sum(Delta_nf_m .^ 2) * sum(Delta_nf_r .^ 2)),
            )
        isnan(y) ? one(y) : y
    end
    results = Optim.optimize(errfun, zeros(N))
    results.minimizer
end

function modal_decomposition_covariance(
    modes_nf::AbstractArray,
    nf::AbstractArray,
    modes_ff::AbstractArray,
    ff::AbstractArray,
)
    @assert length(size(modes_nf)) == length(size(nf)) + 1
    @assert length(size(modes_ff)) == length(size(ff)) + 1
    @assert size(modes_nf)[1:end-1] == size(nf)
    @assert size(modes_ff)[1:end-1] == size(ff)
    N = size(modes_nf)[end]

    function errfun(x)
        nf_rec = msum(abs2.(modes_nf), x)
        Delta_nf_m = nf .- mean(nf)
        Delta_nf_r = nf_rec .- mean(nf_rec)
        ff_rec = msum(abs2.(modes_ff), x)
        Delta_ff_m = ff .- mean(ff)
        Delta_ff_r = ff_rec .- mean(ff_rec)
        C_nf = abs(
            sum(Delta_nf_m .* Delta_nf_r) /
            sqrt(sum(Delta_nf_m .^ 2) * sum(Delta_nf_r .^ 2)),
        )
        C_ff = abs(
            sum(Delta_ff_m .* Delta_ff_r) /
            sqrt(sum(Delta_ff_m .^ 2) * sum(Delta_ff_r .^ 2)),
        )
        y = 2 - C_nf - C_ff
        isnan(y) ? 2 * one(y) : y
    end
    results = Optim.optimize(errfun, zeros(N))
    results.minimizer
end
