module ModeAnalysis

include("utils.jl")
include("rms_optim.jl")
include("covariance_optim.jl")
include("principal_mode_analysis.jl")

export mode_recovery, modal_decomposition_rms, modal_decomposition_covariance

end
